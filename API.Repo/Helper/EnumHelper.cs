﻿namespace API.Repo.Helper
{
    public class EnumHelper
    {
        #region IP Address

        public enum IPAccess
        {
            Allow = 2200,
            Deny = 2201
        }

        #endregion

        #region Application

        public enum App
        {
            AppID = 957850
        }

        #endregion

        #region Gender

        public enum Gender
        {
            Female = 7832,
            Male = 7831
        }
        
        #endregion

        #region Credential Type

        public enum CredentialType
        {
            LiveIQ = 1901,
            Docusign = 1902,
            Users = 1903,
            Company = 1904,
            BackgroundCheck = 1905,
            HireRight = 1906
        }

        #endregion

        #region UserType

        public enum UserType
        {
            Subsource = 9250,
            Manager = 9251,
            User = 9252,
            Employee = 9253
        }

        #endregion
    }
}
