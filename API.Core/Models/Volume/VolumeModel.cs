﻿using API.Core.Interface.ICategory;
using API.Core.Interface.IType;
using API.Core.Interface.IVolume;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Models.Volume
{
    public class VolumeModel : IVolumeDetail,ICategory
    {
        public int VolumeID { get; set; }
        public string Volume { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
    public class VolumeDetailModel : IVolumeDetail, ICategory,IType
    {
        public int VolumeID { get; set; }
        public string Volume { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int TypeID { get; set; }
        public string TypeName { get; set; }
    }

}
