﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.IUser
{
    public interface IContact
    {
        long ContactNo { get; set; }
    }

    public interface IEmail
    {
        string EmailID { get; set; }
    }
}
