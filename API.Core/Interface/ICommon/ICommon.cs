﻿using System;

namespace API.Core.Interface.ICommon
{

    #region App
    public interface IApp
    {
        int AppID { get; set; }
    }

    #endregion

    #region Result

    public interface IStatusID
    {
        int Status { get; set; }
    }

    public interface IMessage
    {
        string Message { get; set; }
    }

    public interface IResult: IStatusID, IMessage
    {

    }

    #endregion

    #region Status

    public interface IActive
    {
        int isActive { get; set; }
    }

    public interface IDeleted
    {
        int isDeleted { get; set; }
    }

    public interface IRecordStatus : IActive, IDeleted
    {

    }

    #endregion

    #region AddressType

    public interface IAddressTypeID
    {
        int AddressTypeID { get; set; }
    }

    public interface IAddressTypeName
    {
        string AddressTypeName { get; set; }
    }

    #endregion

    #region Country

    public interface ICountryID
    {
        int CountryID { get; set; }
    }

    public interface ICountryCode
    {
        string CountryCode { get; set; }
    }

    public interface ICountryName
    {
        string CountryName { get; set; }
    }

    public interface ICountry : ICountryID, ICountryCode, ICountryName
    {

    }

    #endregion

    #region State

    public interface IStateID
    {
        int StateID { get; set; }
    }

    public interface IStateCode
    {
        string StateCode { get; set; }
    }

    public interface IStateName
    {
        string StateName { get; set; }
    }

    public interface IState : IStateID, IStateName
    {
    }

    public interface IStateBasic : IStateID, IStateCode, IStateName
    {
    }


    #endregion

    #region City

    public interface ICityName
    {
        string CityName { get; set; }
    }
    public interface ICityID
    {
        int CityID { get; set; }
    }

    public interface ILat
    {
        string Lat { get; set; }
    }
    public interface ILng
    {
        string Lng { get; set; }
    }

    #endregion

    #region Detail

    public interface IDetail
    {
        string Detail { get; set; }
    }

    #endregion

    #region Created

    public interface ICreatedBy
    {
        long CreatedBy { get; set; }
    }

    public interface ICreatedOn
    {
        DateTime CreatedOn { get; set; }

    }

    public interface ICreated : ICreatedBy, ICreatedOn
    {

    }

    #endregion

    #region Updated

    public interface IUpdatedBy
    {
        long UpdatedBy { get; set; }
    }

    public interface IUpdatedOn
    {
        DateTime UpdatedOn { get; set; }
    }

    public interface IUpdated : IUpdatedBy, IUpdatedOn
    {
    }

    #endregion

    #region AddressType

    public interface IAddressType
    {
        int AddressTypeID { get; set; }
    }

    public interface IAddress
    {
        string Address { get; set; }
    }

    #endregion

    #region isLocked

    public interface IisLocked
    {
        int isLocked { get; set; }
    }

    #endregion

    #region isPrimary

    interface IIsPrimary
    {
        int isPrimary { get; set; }
    }

    #endregion

    #region Gender
    interface IGenderID
    {
        int GenderID { get; set; }
    }
    interface IGenderName
    {
        string GenderName { get; set; }
    }
    #endregion

    #region Type

    interface ITypeID
    {
        int TypeID { get; set; }
    }

    interface ITypeName
    {
        string TypeName { get; set; }
    }

    #endregion

}
