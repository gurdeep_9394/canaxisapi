﻿using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;

namespace API.Core.Models.User
{
    public class UserAddress : IAddressLine1, IAddressLine2,IPinCode,ICityName, IStateID, IStateName, ICountryID, ICountryCode, ICountryName
    {
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string CityName { get; set; }

        public int StateID { get; set; }

        public string StateName { get; set; }

        public int CountryID { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public long PinCode { get; set; }
    }
    
}
