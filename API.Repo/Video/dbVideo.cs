﻿using API.Repo.Helper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;

namespace API.Repo.Video
{
  public class dbVideo
    {
        public int VideoCreate( string VideoName, string VideoDetail, string VideoLink, int FileTypeID, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("videoDetailCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@VideoName", SqlDbType.VarChar, 100).Value = VideoName;
                    cmd.Parameters.Add("@VideoDetail", SqlDbType.VarChar, 500).Value = VideoDetail;
                    cmd.Parameters.Add("@VideoLink", SqlDbType.VarChar, 500).Value = VideoLink;
                    
                    cmd.Parameters.Add("@FileTypeID", SqlDbType.Int).Value = FileTypeID;
                  
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
    }
}
