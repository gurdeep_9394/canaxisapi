﻿using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace API.Repo.Modules
{
    public class dbModules
    {
        public DataTable moduleReturn(decimal reqid, int appid, string token, int ModuleID, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("moduleReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid, token);
                    cmd.Parameters.Add("@ModuleID", SqlDbType.Int).Value = ModuleID;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
    }
}
