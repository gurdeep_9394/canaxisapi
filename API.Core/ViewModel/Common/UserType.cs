﻿using API.Core.Interface.ICommon;
using API.Core.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.ViewModel.Common
{
    public class UserTypeView : IResult
    {
        public List<UserType> Detail { get; set; }
        public string Message { get; set; }       

        public int Status { get; set; }
    }
}
