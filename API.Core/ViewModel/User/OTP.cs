﻿using API.Core.Interface.ICommon;

using API.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.ViewModel.User
{
    public class OTPDetail
    {
        public string OTP { get; set; }
    }
    public class OTPView
    {
        public OTPDetail Basic { get; set; }
        
    }
    public class OTPViewModel : IResult
    {
        public List<OTPView> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }

    }
    //public class UserOTPViewModel : IResult
    //{
    //    public List<UserOTPDetail> DetailObject { get; set; }
    //    public int Status { get; set; }
    //    public string Message { get; set; }

    //}
}
