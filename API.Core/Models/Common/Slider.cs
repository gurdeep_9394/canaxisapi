﻿using API.Core.Interface.ICommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Models.Common
{
    public class SliderDetail : IImageID, IImageName, IPriorityID
    {
        public int ImageID { get; set; }
        public string ImageName { get; set; }
        public int PriorityID { get; set; }
    }
}
