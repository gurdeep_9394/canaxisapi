﻿using API.Core.Models.TestType;
using API.Core.ViewModel.TestType;
using API.Repo.Helper;
using API.Repo.TestType;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace dbackers.Controllers
{/// <summary>
/// Type
/// </summary>
    public class TestTypeController : ApiController
    {
        /// <summary>
        /// Type Return
        /// </summary>
        /// <param name="typeid"></param>
        /// <returns></returns>
        [Route("api/TestType/TestTypeReturn")]
        [HttpGet]
        public IHttpActionResult TestTypeReturn(int typeid)
        {
            TestTypeViewModel user = new TestTypeViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                dbTestType ua = new dbTestType();
                int stat = -1;
                string msg = "";
                List<TestTypeModel> Detail = new List<TestTypeModel>();
                DataTable detail = ua.testTypeReturn(reqid, appid, token, typeid, userid, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new TestTypeModel
                                  {
                                      TestTypeID = string.IsNullOrEmpty(item["TestTypeID"].ToString()) ? 0 : Convert.ToInt32(item["TestTypeID"]),
                                      TestType = item.Field<string>("TestType"),

                                  }).ToList();
                        user.Details = Detail;
                        user.Message = "Test type return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.Details = null;
                        user.Message = "No Test type is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.Details = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.Details = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
    }
}
