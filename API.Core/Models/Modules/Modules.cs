﻿using API.Core.Interface.IModules;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Models.Modules
{
    public class ModulesDetails : IModules
    {
        public int ModulesID { get; set; }
        public string ModulesName { get; set; }
    }
}
