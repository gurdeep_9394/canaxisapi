﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace API.Repo.Helper
{
    public class EncryptHelperObj
    {
        public string SaltKey { get; set; }

        public string SaltKeyIV { get; set; }

        public string Value { get; set; }

        public byte[] EncryptString { get; set; }
    }

    public class EncryptHelper
    {
        public static EncryptHelperObj Encrypt_NewHelper()
        {
            EncryptHelperObj _EncryptHelperObj = new EncryptHelperObj();
            Aes myAes = Aes.Create();
            _EncryptHelperObj.SaltKey = Encoding.Default.GetString(myAes.Key);
            _EncryptHelperObj.SaltKeyIV = Encoding.Default.GetString(myAes.IV);
            return _EncryptHelperObj;
        }

        public static bool CheckStringISNormal(string inputstring)
        {
            //inputstring = "[Q°{1¾¬µâjSAôxáRA/£ûÚJN^´€8,&";//"Mar 20 1965 12:00AM";// 
            bool valueIsUnknown = false;
            if (!string.IsNullOrEmpty(inputstring))
            {
                Regex regex = new Regex(@"[A-Za-z0-9 .,--_^!%$~#<>?"":;'`@&$*/=+(){}\[\]\\]");
                MatchCollection matches = regex.Matches(inputstring);

                if (matches.Count.Equals(inputstring.Length))
                { valueIsUnknown = true; }
                else
                { valueIsUnknown = false; }
            }
            return valueIsUnknown;
        }

        public byte[] EncryptString_New(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                //ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }
        public string DecryptString_New(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting                             stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;
        }

        public static EncryptHelperObj Get_EncryptedPassword(EncryptHelperObj _EncryptHelperObj, string Password)
        {
            if (CheckStringISNormal(Password))
            {
                try
                {
                    if (Password.Length > 0)
                    {
                        if (_EncryptHelperObj == null || _EncryptHelperObj.SaltKey == null || _EncryptHelperObj.SaltKey.Length <= 0)
                        {
                            _EncryptHelperObj = EncryptHelper.Encrypt_NewHelper();
                        }
                        EncryptHelper eh = new EncryptHelper();
                        _EncryptHelperObj.EncryptString = eh.EncryptString_New(Password, Encoding.Default.GetBytes(_EncryptHelperObj.SaltKey), Encoding.Default.GetBytes(_EncryptHelperObj.SaltKeyIV));
                    }
                    else
                    { _EncryptHelperObj.EncryptString = null; }
                }
                catch
                {
                    _EncryptHelperObj.EncryptString = null;
                }
                if (_EncryptHelperObj.EncryptString != null)
                {
                    _EncryptHelperObj.Value = Encoding.Default.GetString(_EncryptHelperObj.EncryptString);
                }
            }
            else
            { _EncryptHelperObj.Value = Password; }

            return _EncryptHelperObj;
        }
    }
}
