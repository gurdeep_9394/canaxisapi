﻿using API.Core.Models.Category;
using API.Core.Models.Common;
using API.Core.Models.TestType;
using API.Core.ViewModel.Category;
using API.Repo.Category;
using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace dbackers.Controllers
{/// <summary>
/// category
/// </summary>
    public class CategoryController : ApiController
    {
        /// <summary>
        /// create
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Route("api/Category/CategoryCreate")]
        [HttpPost]

        public IHttpActionResult CategoryCreate(CategoryCreate obj)
        {
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }
                if (headers.Contains("token"))
                {
                    token = Convert.ToString(headers.GetValues("token").First());
                }
                string message = "";

                dbCategory volume = new dbCategory();
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);


                if (volume.testCategoryCreate(reqid, appid, obj.CategoryName, obj.TestTypeID, userid, token, out message) == 1)
                {
                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }

        /// <summary>
        /// Category return
        /// </summary>
        /// <param name="typeid"></param>
        /// <param name="categoryid"></param>
        /// <returns></returns>

        [Route("api/Category/CategoryReturn")]
        [HttpGet]
        public IHttpActionResult CategoryReturn(int typeid,int categoryid)
        {
            CategoryViewModel user = new CategoryViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                dbCategory ua = new dbCategory();
                int stat = -1;
                string msg = "";
                List<CategoryModel> Detail = new List<CategoryModel>();
                DataTable detail = ua.testCategoryDetail(reqid, appid, token, typeid, categoryid, userid, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new CategoryModel
                                  {
                                      TestTypeID = string.IsNullOrEmpty(item["TestTypeID"].ToString()) ? 0 : Convert.ToInt32(item["TestTypeID"]),
                                      TestType = item.Field<string>("TestType"),
                                      CategoryID = string.IsNullOrEmpty(item["CategoryID"].ToString()) ? 0 : Convert.ToInt32(item["CategoryID"]),
                                      CategoryName = item.Field<string>("CategoryName"),

                                  }).ToList();
                        user.Details = Detail;
                        user.Message = "Category return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.Details = null;
                        user.Message = "No Category is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.Details = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.Details = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
    }
}
