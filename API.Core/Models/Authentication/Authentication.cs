﻿using System;
using API.Core.Interface.IAuthentication;
using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;


namespace API.Core.Models.Authentication
{
    public class UserAccount: IUserName, IPassMail
    {
        public string UserName { get; set; }

        public string PassMail { get; set; }
    }

    public class UserLogin : UserAccount, IPasskey
    {
        public string Passkey { get; set; }
    }

    public class UserAccountPasskey: IPasskey, ISaltKeys
    {
        public string Passkey { get; set; }

        public string SaltKey { get; set; }

        public string SaltKeyIV { get; set; }
    }

    public class UserAccountNewPasskey: IPasskey
    {
        public string Passkey { get; set; }

        public string NewPasskey { get; set; }

    }

    public class UserLoginDetail : IUserID,IUserName,IUserTypeName
        {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserTypeName { get; set; }
    }

    public class ForgetPassword : IPasskey,IUserID
    {
        public string Passkey { get; set; }

        public long UserID { get; set; }

        public string OTP { get; set; }
    }
    public class Login : IContactNo, IPasskey
    {
        public long ContactNo { get; set; }

        public string Passkey { get; set; }
    }
    public class SignOut : IUserID
    {
        public long UserID { get; set; }
    }

}
