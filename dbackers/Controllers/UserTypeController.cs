﻿using API.Core.Models.Common;
using API.Core.ViewModel.Common;
using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ProjectX.WebAPI.Controllers
{
    /// <summary>
    /// UserType
    /// </summary>
    public class UserTypeController : ApiController
    {
        /// <summary>
        /// Return
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Return(int id, int isActive)
        {
            UserTypeView user = new UserTypeView();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();
                int stat = -1;
                string msg = "";
                List<UserType> Detail = new List<UserType>();
                DataTable detail = ua.userTypeReturn(reqid, appid, id, isActive, 0, userid, token, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new UserType
                                  {
                                      UserTypeID = string.IsNullOrEmpty(item["TypeID"].ToString()) ? 0 : Convert.ToInt32(item["TypeID"]),
                                      UserTypeName = item.Field<string>("TypeName")
                                  }).ToList();
                        user.Detail = Detail;
                        user.Message = "User Type return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.Detail = null;
                        user.Message = "No user type is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.Detail = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.Detail = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
    }
}
