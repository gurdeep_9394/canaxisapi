using System.Web.Http;
using WebActivatorEx;
using dbackers;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "SwaggerRegister")]

namespace dbackers
{
    ///<Summary>
    /// Gets the answer
    ///</Summary>
    public class SwaggerConfig
    {
        ///<Summary>
        /// Gets the answer
        ///</Summary>
        public static void SwaggerRegister()
        {
            
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                 .EnableSwagger(c =>
                 {
                     c.SingleApiVersion("v1", "dbackersAPI");
                     c.IncludeXmlComments(GetXmlCommentsPath());
                 })
                 .EnableSwaggerUi(c => c.DisableValidator());

        }
        /// <summary>
        /// Get XML
        /// </summary>
        /// <returns></returns>
        protected static string GetXmlCommentsPath()
        {
            return System.String.Format(@"{0}\bin\dbackers.XML",
                System.AppDomain.CurrentDomain.BaseDirectory);
        }
    }
}
