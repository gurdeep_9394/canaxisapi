﻿using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;
using API.Core.Models.Authentication;
using API.Core.Models.Common;
using API.Core.ViewModel.User;


namespace API.Core.ViewModel.Authentication
{
    public class UserLoginAuthViewModel : IResult
    {
        public UserAccountPasskey PassKey { get; set; }

        public UserLoginView Login { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }
    }
    public class UserLoginViewModel : IUserID, IResult, IToken
    {
        public string Token { get; set; }

        public UserType Detail { get; set; }

        public long UserID { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }
    }
}
