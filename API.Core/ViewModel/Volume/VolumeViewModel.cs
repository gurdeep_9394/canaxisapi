﻿using API.Core.Interface.ICommon;
using API.Core.Models.Volume;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.ViewModel.Volume
{
    public class VolumeViewModel:IResult
    {
        public List<VolumeDetailModel> Detail { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
