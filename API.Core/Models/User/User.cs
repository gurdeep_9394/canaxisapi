﻿using System;
using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;
using sh.Core.Interface.IOTP;

namespace API.Core.Models.User
{
    public class UserBasic : IUserID, IFirstName, ILastName
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public long UserID { get; set; }
    }

    public class UserOTPDetail : IUserID, IOTP
    {
        public string OTP { get; set; }
        public long UserID { get; set; }
    }
    public class UserProfile : IUserTypeID, IUserTypeName,ICityID,ICityName,IUserID,ILat,ILng,IDetail
    {
        public long UserID { get; set; }
        public int UserTypeID { get; set; }
        public string UserTypeName { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public long ContactNo { get; set; }
        public string AddressLine { get; set; }
        public string ZipCode { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string MobileDetail { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Detail { get; set; }

    }

    public class UserBasicProfile :  ICityID, IUserID,IUserTypeID
    {
        public long UserID { get; set; }      
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string EmailID { get; set; }
        public long ContactNo { get; set; }
        public string AddressLine { get; set; }
        public string ZipCode { get; set; }
        public int CityID { get; set; }
        public string MobileDetail { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public int UserTypeID { get; set; }
    }

    public class UserBasicDetail: IUserID
    {
        public long UserID { get; set; }
        public string FullName { get; set; }
        public string Image { get; set; }

    }

    public class WalletReturn : IUserID
    {
        public long UserID { get; set; }
        public decimal TotalAmount { get; set; }
    }


}
