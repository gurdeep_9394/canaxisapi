﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface.ICommon
{
    
    interface IPaymentModeID
    {
        int PaymentModeID { get; set; }
    }
    interface IPaymentMode
    {
        string PaymentMode { get; set; }
    }
 
}
