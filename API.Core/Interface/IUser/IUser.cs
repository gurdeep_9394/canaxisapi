﻿namespace API.Core.Interface.IUser
{
    public interface IUserID
    {
        long UserID { get; set; }
    }

    public interface IOperand
    {
        long Operand { get; set; }
    }

    public interface IToken
    {
        string Token { get; set; }
    }

    public interface IFirstName
    {
        string FirstName { get; set; }
    }

    public interface ILastName
    {
        string LastName { get; set; }
    }

    //public interface IUserName
    //{
    //    string UserName { get; set; }
    //}

    #region Contact Detail

    public interface IContactNo
    {
        long ContactNo { get; set; }
    }

    public interface IEmailID
    {
        string EmailID { get; set; }
    }

    #endregion

    public interface IAppVersion
    {
        string AppVersion { get; set; }
    }

}
