﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.IType
{
    public interface IType : ITypeID, ITypeName
    {
    }
    public interface ITypeID
    {
        int TypeID { get; set; }
    }
    public interface ITypeName
    {
        string TypeName { get; set; }
    }
}
