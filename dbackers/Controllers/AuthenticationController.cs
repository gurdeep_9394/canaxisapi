﻿using API.Core.Models.Authentication;
using API.Core.Models.Common;
using API.Core.Models.User;
using API.Core.ViewModel.Authentication;
using API.Core.ViewModel.User;
using API.Core.ViewModel.OTP;
using API.Repo.Helper;
using API.Repo.User;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace web.Api.Controllers
{
    /// <summary>
    /// Auth
    /// </summary>
    public class AuthenticationController : ApiController
    {
       
      /// <summary>
      /// login
      /// </summary>
      /// <param name="obj"></param>
      /// <returns></returns>
        [Route("api/Authentication/login")]
        [HttpPost]
        // POST: api/Authentication
        public IHttpActionResult login(Login obj)
        {
            UserLoginViewModel currentUser = new UserLoginViewModel();
            try
            {
                UserLoginAuthViewModel Login = new UserLoginAuthViewModel();
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;

                int appid = -1;
                long userid = -1;

                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }


                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                userAuthentication sa = new userAuthentication();

                int stat = -1;
                string msg = "";

                UserAccountPasskey Detail = new UserAccountPasskey();
                UserLoginView DetailType = new UserLoginView();

                DataTable dtlogin = sa.userLoginContact( obj.ContactNo, out stat, out msg);

                Login.Message = msg;
                Login.Status = stat;

                if (stat == 1)
                {
                    if (dtlogin != null)
                    {
                        Detail = (from item in dtlogin.AsEnumerable()
                                  select new UserAccountPasskey
                                  {
                                      SaltKey = item.Field<string>("SaltKey"),
                                      SaltKeyIV = item.Field<string>("SaltKeyIV"),
                                      Passkey = item.Field<string>("Passkey"),
                                  }).FirstOrDefault();
                        DetailType = (from item in dtlogin.AsEnumerable()
                                      select new UserLoginView
                                      {
                                          Type = new UserType
                                          {
                                              UserTypeID = item.Field<int>("TypeID"),
                                              UserTypeName = item.Field<string>("TypeName"),

                                          },
                                          UserID = item.Field<long>("UserID")
                                      }).FirstOrDefault();

                        //EncryptHelper eh = new EncryptHelper();
                        EncryptHelper eh = new EncryptHelper();
                        byte[] encPas = eh.EncryptString_New(obj.Passkey, Encoding.Default.GetBytes(Detail.SaltKey), Encoding.Default.GetBytes(Detail.SaltKeyIV));
                        byte[] pas = Encoding.Default.GetBytes(Detail.Passkey);

                        userid = DetailType.UserID;

                        if (StructuralComparisons.StructuralEqualityComparer.Equals(encPas, pas))
                        {
                            Login.Login = DetailType;
                            string tokenid = "";
                            int check = 0;
                            do
                            {
                                tokenid = Guid.NewGuid().ToString();
                                tokenid = tokenid.Replace("-", "");
                                check = sa.userAuthenticationTokenCreate(reqid, tokenid, userid, out msg);
                                if (msg != "Token already exists")
                                {
                                    currentUser.Message = msg;
                                    break;
                                }
                            }
                            while (check != 1);
                            if (check == 0)
                            {
                                currentUser.Detail = DetailType.Type;
                                currentUser.Token = "NA";
                                currentUser.UserID = userid;
                                currentUser.Message = currentUser.Message;
                                currentUser.Status = check;
                            }
                            else
                            {
                                currentUser.Detail = DetailType.Type;
                                currentUser.Token = tokenid;
                                currentUser.UserID = userid;
                                currentUser.Message = "User logged in successfully!";
                                currentUser.Status = 1;
                            }
                        }
                        else
                        {
                            currentUser.Detail = null;
                            currentUser.Token = "";
                            currentUser.UserID = -1;
                            currentUser.Message = "Mobile Number or Password does not match. Please try again.";
                            currentUser.Status = 0;
                        }
                    }
                    else
                    {
                        currentUser.Detail = null;
                        currentUser.Token = "";
                        currentUser.UserID = -1;
                        currentUser.Message = "Username or Password does not match. Please try again.";
                        currentUser.Status = 0;
                    }

                   

                    return Ok(currentUser);
                }
                else
                {
                    currentUser.Detail = null;
                    currentUser.Token = "";
                    currentUser.UserID = -1;
                    currentUser.Message = msg;
                    currentUser.Status = stat;

                    return Ok(currentUser);
                }
            }
            catch (Exception ex)
            {
                currentUser.Detail = null;
                currentUser.Token = "";
                currentUser.UserID = -1;
                currentUser.Message = ex.Message.ToString();
                currentUser.Status = 0;
                return Content(HttpStatusCode.BadRequest, currentUser);
            }
        }
        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="ul"></param>
        /// <returns></returns>

        [Route("api/Authentication/ChangePassword")]
        [HttpPost]
        // POST: api/Authentication
        public IHttpActionResult ChangePassword(UserAccountNewPasskey ul)
        {
            Result Result = new Result();
            try
            {
                if (ModelState.IsValid)
                {
                    HttpRequestMessage request = Request;
                    HttpRequestHeaders headers = request.Headers;
                    UserAccountPasskey Detail = new UserAccountPasskey();
                    int appid = -1;
                    long userid = -1;
                    string token = "NA";
                    if (headers.Contains("appid"))
                    {
                        appid = Convert.ToInt32(headers.GetValues("appid").First());
                    }
                    if (headers.Contains("token"))
                    {
                        token = headers.GetValues("token").First();
                    }
                    if (headers.Contains("userid"))
                    {
                        userid = Convert.ToInt64(headers.GetValues("userid").First());
                    }
                    if (appid != -1 && userid != -1 && token != "NA")
                    {
                        int stat = -1;
                        string msg = "";
                        RequestHelper sr = new RequestHelper();
                        decimal reqid = sr.requestUniqueID(userid);
                        userAuthentication sa = new userAuthentication();
                        DataTable dtpasskey = sa.userPasskeyReturn(reqid, appid, token, userid, out stat, out msg);
                        if (stat == 1)
                        {
                            if (dtpasskey != null)
                            {
                                Detail = (from item in dtpasskey.AsEnumerable()
                                          select new UserAccountPasskey
                                          {
                                              SaltKey = item.Field<string>("SaltKey"),
                                              SaltKeyIV = item.Field<string>("SaltKeyIV"),
                                              Passkey = item.Field<string>("Passkey"),
                                          }).FirstOrDefault();
                                EncryptHelper eh = new EncryptHelper();
                                byte[] encPas = eh.EncryptString_New(ul.Passkey, Encoding.Default.GetBytes(Detail.SaltKey), Encoding.Default.GetBytes(Detail.SaltKeyIV));
                                byte[] pas = Encoding.Default.GetBytes(Detail.Passkey);
                                //string oldpasskey = eh.DecryptString_New( Encoding.Default.GetBytes(Detail.Passkey), Encoding.Default.GetBytes(Detail.SaltKey), Encoding.Default.GetBytes(Detail.SaltKeyIV));
                                if (StructuralComparisons.StructuralEqualityComparer.Equals(encPas, pas))
                                {
                                    EncryptHelperObj _objuserEncryptHelperObj = new EncryptHelperObj();
                                    _objuserEncryptHelperObj = EncryptHelper.Get_EncryptedPassword(_objuserEncryptHelperObj, ul.NewPasskey);
                                    string NewPasskey = _objuserEncryptHelperObj.Value;
                                    string NewSaltKey = _objuserEncryptHelperObj.SaltKey;
                                    string NewSaltKeyIV = _objuserEncryptHelperObj.SaltKeyIV;

                                    //if(oldpasskey==ul.Key.Passkey)
                                    //{
                                    if (sa.UserAccountPasskeyUpdate(reqid, appid, userid, NewPasskey, NewSaltKey, NewSaltKeyIV, token, userid, out msg) == 1)
                                    {
                                        Result.Status = 1;
                                        Result.Message = msg;

                                        return Content(HttpStatusCode.OK, Result);
                                    }
                                    else
                                    {
                                        Result.Status = 0;
                                        Result.Message = msg;

                                        return Ok(Result);
                                    }
                                }
                                else
                                {
                                    Result.Status = 0;
                                    Result.Message = "Password do not match! Please use correct password.";

                                    return Ok(Result);
                                }
                            }
                            else
                            {
                                Result.Status = 0;
                                Result.Message = "Password do not match! Please use correct password.";

                                return Ok(Result);
                            }
                        }
                        else
                        {
                            Result.Status = 0;
                            Result.Message = "Old password does not exists";

                            return Ok(Result);
                        }
                    }
                    else
                    {
                        Result.Status = 0;
                        Result.Message = "Invalid request headers!";

                        return Content(HttpStatusCode.OK, Result);
                    }
                }
                else
                {
                    Result.Status = 0;
                    Result.Message = "Invalid data passed to the end point!";

                    return Content(HttpStatusCode.BadRequest, Result);
                }
            }
            catch (Exception ex)
            {
                Result.Status = 0;
                Result.Message = ex.Message.ToString();

                return Content(HttpStatusCode.BadRequest, Result);
            }
        }

        /// <summary>
        /// Forget Password
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Route("api/Authentication/ForgetPasswordChange")]
        [HttpPost]

        public IHttpActionResult ForgetPasswordChange(ForgetPassword obj)
        {
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }
                string message = "";
                UserAccountPasskey _objUsers = new UserAccountPasskey();
                EncryptHelperObj _objuserEncryptHelperObj = new EncryptHelperObj();
                _objuserEncryptHelperObj = EncryptHelper.Get_EncryptedPassword(_objuserEncryptHelperObj, obj.Passkey);
                _objUsers.Passkey = _objuserEncryptHelperObj.Value;
                _objUsers.SaltKey = _objuserEncryptHelperObj.SaltKey;
                _objUsers.SaltKeyIV = _objuserEncryptHelperObj.SaltKeyIV;
                userAuthentication ua = new userAuthentication();

                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);


                if (ua.userForgetPasskeyReset(reqid, appid, obj.UserID, obj.OTP, _objUsers.Passkey, _objUsers.SaltKey, _objUsers.SaltKeyIV, userid, out message) == 1)
                {
                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }

        #region ForgetPasswordReturn
        /// <summary>
        /// ForgetPassword
        /// </summary>
        /// <param name="ContactNo"></param>
        /// <returns></returns>

        [Route("api/Authentication/ForgetPassword")]
        [HttpGet]
        public IHttpActionResult ForgetPassword(long ContactNo)
        {
            UserOTPViewModel user = new UserOTPViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                userAuthentication ua = new userAuthentication();
                int stat = -1;
                string msg = "";
                List<UserOTPDetail> Detail = new List<UserOTPDetail>();
                DataTable detail = ua.userForgetPasskey(reqid, appid, ContactNo, userid, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        string OTP = ua.OTP();
                        reqid = sr.requestUniqueID(userid);
                        long operand = Convert.ToInt64(detail.Rows[0]["UserID"]);
                        if (ua.userOTPCreate(reqid, appid, operand, OTP, 1, userid, out msg) == 1)
                        {
                            string OTPMessage = "Your One Time Password is: " + OTP + " to proceed on MrFreshEats. It is Valid for 30 minutes. Thank you for choosing us.\n\nMrFreshEats";
                            ua.SendSMS(ContactNo, OTPMessage);
                        }

                        Detail = (from item in detail.AsEnumerable()
                                  select new UserOTPDetail
                                  {
                                      UserID = string.IsNullOrEmpty(item["UserID"].ToString()) ? 0 : Convert.ToInt64(item["UserID"]),
                                      OTP = OTP
                                  }).ToList();

                        user.DetailObject = Detail;
                        user.Message = "User Detail return successfully!";
                        user.Status = 1;

                    }
                    else
                    {
                        user.DetailObject = null;
                        user.Message = "No user is found!";
                        user.Status = 1;
                    }

                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.DetailObject = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.DetailObject = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
        #endregion


        #region Signout
        /// <summary>
        /// SignOut
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Route("api/Authentication/SignOut")]
        [HttpPost]
        public IHttpActionResult SignOut(SignOut obj)
        {
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                string message = "";
                UserAccountPasskey _objUsers = new UserAccountPasskey();
                userAuthentication ua = new userAuthentication();

                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);


                if (ua.userSignOut(reqid, appid, obj.UserID, token, userid, out message) == 1)
                {
                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }
        #endregion

 
       
    }
}
