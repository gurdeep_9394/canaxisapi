﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.TestType
{
    public interface ITestTypeDetails: ITestTypeID, ITestType
    {
    }
    public interface ITestTypeID
    {
        int TestTypeID { get; set; }
    }
    public interface ITestType
    {
        string TestType { get; set; }
    }
}
