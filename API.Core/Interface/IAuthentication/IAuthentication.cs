﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.IAuthentication
{
    public interface IUserName
    {
        string UserName { get; set; }
    }

    public interface IPassMail
    {
        string PassMail { get; set; }
    }

    public interface IPasskey
    {
        string Passkey { get; set; }
    }

    public interface ISaltKey
    {
        string SaltKey { get; set; }
    }

    public interface ISaltKeyIV
    {
        string SaltKeyIV { get; set; }
    }

    public interface ISaltKeys: ISaltKey, ISaltKeyIV
    {
    }

}
