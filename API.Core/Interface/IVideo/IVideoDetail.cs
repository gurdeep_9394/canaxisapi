﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sh.Core.Interface.IVideo
{
    public interface IVideo: IVideoDetail, IVideoID, IVideoLink, IVideoName
    {

    }
    public interface IVideoDetail
    {
        string VideoDetail { get; set; }
    }
    public interface IVideoID
    {
        int VideoID { get; set; }
    }
    public interface IVideoLink
    {
        string VideoLink { get; set; }
    }
    public interface IVideoName
    {
        string VideoName { get; set; }
    }
}
