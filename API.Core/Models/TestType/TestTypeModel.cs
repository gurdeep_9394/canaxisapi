﻿using API.Core.Interface.TestType;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Models.TestType
{
    public class TestTypeModel : ITestTypeDetails
    {
        public int TestTypeID { get; set; }
        public string TestType { get; set; }
    }
}
