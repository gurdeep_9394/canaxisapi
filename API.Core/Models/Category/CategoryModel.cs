﻿using API.Core.Interface.ICategory;
using API.Core.Interface.TestType;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Models.Category
{
   public class CategoryModel : ICategory, ITestTypeDetails
    {
        public int CategoryID { get ; set ; }
        public string CategoryName { get; set; }
        public int TestTypeID { get; set; }
        public string TestType { get; set; }

    }
    public class CategoryCreate:ICategoryName,ITestTypeID
    {
        public int TestTypeID { get; set; }
        public string CategoryName { get; set; }
    }
}
