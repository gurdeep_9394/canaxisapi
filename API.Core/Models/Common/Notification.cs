﻿using API.Core.Interface.ICommon;


namespace API.Core.Models.Common
{
   public class NotificationCounter:INotificationCount
    {
        public int NotificationCount { get; set; }
    }

    //public class NotificationDetail : INotificationID,INotificationText,INotificationType,IOrderID,ITypeID
    //{
    //    public long NotificationID { get; set; }
    //    public string NotificationText { get; set; }
    //    public string NotificationType { get; set; }
    //    public int isRead { get; set; }
    //    public int OrderID { get; set; }
    //    public int TypeID { get; set; }
    //}



}
