﻿
using API.Core.ViewModel.User;
using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ProjectX.Repository.User;
using API.Core.Models.User;
using API.Core.Models.Common;
using API.Core.Models.Authentication;
using API.Core.ViewModel.Authentication;

namespace web.Api.Controllers
{/// <summary>
/// 
/// </summary>
    public class UserDetailController : ApiController
    {
        /// <summary>
        /// Return
        /// </summary>
        /// <param name="operand"></param>
        /// <returns></returns>
        #region Return
        [HttpGet]
        public IHttpActionResult Return(long operand)
        {
            UserDetailViewModel user = new UserDetailViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();
                int stat = -1;
                string msg = "";
                List<UserDetailView> Detail = new List<UserDetailView>();
                DataTable detail = ua.userProfileReturn(reqid, appid, operand,userid,token, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new UserDetailView
                                  {
                                      Basic = new UserProfile()
                                      {
                                          ContactNo = string.IsNullOrEmpty(item["ContactNo"].ToString()) ? 0 : Convert.ToInt64(item["ContactNo"]),
                                          FullName = item.Field<string>("FullName"),                                         
                                          AddressLine = item.Field<string>("AddressLine"),
                                          EmailID = item.Field<string>("EmailID"),
                                          UserName = item.Field<string>("UserName"),
                                          Image = "/Documents/Profile/" + Convert.ToString(string.IsNullOrEmpty(item.Field<string>("ImageURL")) ? " " : item.Field<string>("ImageURL")),
                                          ZipCode = item.Field<string>("ZipCode"),
                                          Lng = item.Field<string>("Lng"),
                                          Lat = item.Field<string>("Lat"),
                                          Detail = item.Field<string>("Detail"),

                                      }
                                  }).ToList();
                        user.DetailObject = Detail;
                        user.Message = "User Deatil return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.DetailObject = null;
                        user.Message = "No user is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.DetailObject = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.DetailObject = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
        #endregion

        #region Create
        /// <summary>
        /// profileCreate
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult profileCreate(UserBasicProfile user)
        {
            UserLoginViewModel currentUser = new UserLoginViewModel();
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;

                long userid = -1;
                
                string message = "";

                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();
                userid = ua.userUniqueID();
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                UserAccountPasskey _objUsers = new UserAccountPasskey();
                EncryptHelperObj _objuserEncryptHelperObj = new EncryptHelperObj();
                _objuserEncryptHelperObj = EncryptHelper.Get_EncryptedPassword(_objuserEncryptHelperObj, user.Password);
                _objUsers.Passkey = _objuserEncryptHelperObj.Value;
                _objUsers.SaltKey = _objuserEncryptHelperObj.SaltKey;
                _objUsers.SaltKeyIV = _objuserEncryptHelperObj.SaltKeyIV;

                if (ua.userAccountCreate(userid, user.UserName, user.FullName, user.AddressLine, user.ZipCode, user.CityID, user.ContactNo, user.EmailID, _objUsers.Passkey, _objUsers.SaltKey, _objUsers.SaltKeyIV, user.UserTypeID, user.MobileDetail,user.Image, userid, out  message) == 1)
                {
                   
                    reqid = sr.requestUniqueID(userid);
                    currentUser.Token = "NA";
                    currentUser.UserID = userid;
                    currentUser.Message = message;
                    currentUser.Status = 1;
                    return Content(HttpStatusCode.OK, currentUser);
                }
                else
                {
                    currentUser.Token = "NA";
                    currentUser.UserID = -1;
                    currentUser.Message = message;
                    currentUser.Status = 0;
                    
                    return Content(HttpStatusCode.OK, currentUser);
                }
            }
            catch (Exception ex)
            {
                currentUser.Token = "NA";
                currentUser.UserID = -1;
                currentUser.Message = ex.ToString(); 
                currentUser.Status = 0;
                return Content(HttpStatusCode.BadRequest, currentUser);
            }
        }

        #endregion

        #region Update
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="basic"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Update(UserBasicDetail basic)
        {
            
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }
                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                string message = "";

                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();             
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                

                if (ua.userProfileEdit(reqid, appid, basic.Image, basic.FullName, userid, token, out message) == 1)
                {

                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }

        #endregion

    }
}
