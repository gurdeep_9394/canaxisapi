﻿using API.Core.Interface.ICommon;

using API.Core.Models.AdminApp;
using System.Collections.Generic;

namespace API.Core.ViewModel.AdminApp
{
    public class DashboardCountViewModel : IStatusID, IMessage
    {
        public List<DashboardCount> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
