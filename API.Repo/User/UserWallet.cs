﻿using API.Repo.Helper;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ProjectX.Repository.User
{
 public   class UserWallet
    {
        #region Wallet
        public DataTable userWalletReturn(decimal reqid, int appid, long operand, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userWalletReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = operand;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        #endregion
        #region Wallet
        public DataTable transactionDetailReturn(decimal reqid, int appid, long operand,int typeid,int modeid, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("transactionDetailReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = typeid;
                    cmd.Parameters.Add("@PaymentModeID", SqlDbType.Int).Value = modeid;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        #endregion
    }
}
