﻿using API.Core.ViewModel.User;
using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ProjectX.Repository.User;
using API.Core.Models.User;
using API.Core.Models.Common;
using API.Core.Models.Authentication;
using API.Core.ViewModel.Authentication;

namespace web.Api.Controllers
{/// <summary>
/// UserAccount
/// </summary>
    public class UserAccountController : ApiController
    {
       /// <summary>
       /// post
       /// </summary>
       /// <param name="user"></param>
       /// <returns></returns>

        // POST: api/UserAccount
        public IHttpActionResult Post(UserBasicProfile user)
        {
            UserLoginViewModel currentUser = new UserLoginViewModel();
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }
                string message = "";

                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();
                userid = ua.userUniqueID();
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                UserAccountPasskey _objUsers = new UserAccountPasskey();
                EncryptHelperObj _objuserEncryptHelperObj = new EncryptHelperObj();
                _objuserEncryptHelperObj = EncryptHelper.Get_EncryptedPassword(_objuserEncryptHelperObj, user.Password);
                _objUsers.Passkey = _objuserEncryptHelperObj.Value;
                _objUsers.SaltKey = _objuserEncryptHelperObj.SaltKey;
                _objUsers.SaltKeyIV = _objuserEncryptHelperObj.SaltKeyIV;

                if (ua.userAccountCreate(userid, user.UserName, user.FullName, user.AddressLine, user.ZipCode, user.CityID, user.ContactNo, user.EmailID, _objUsers.Passkey, _objUsers.SaltKey, _objUsers.SaltKeyIV, user.UserTypeID, user.MobileDetail, user.Image, userid, out message) == 1)
                {

                    reqid = sr.requestUniqueID(userid);
                    currentUser.Token = "NA";
                    currentUser.UserID = userid;
                    currentUser.Message = message;
                    currentUser.Status = 1;
                    return Content(HttpStatusCode.OK, currentUser);
                }
                else
                {
                    currentUser.Token = "NA";
                    currentUser.UserID = -1;
                    currentUser.Message = message;
                    currentUser.Status = 0;

                    return Content(HttpStatusCode.OK, currentUser);
                }
            }
            catch (Exception ex)
            {
                currentUser.Token = "NA";
                currentUser.UserID = -1;
                currentUser.Message = ex.ToString();
                currentUser.Status = 0;
                return Content(HttpStatusCode.BadRequest, currentUser);
            }
        }

       
    }
}
