﻿using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace API.Repo.Volume
{
    public class dbVolume
    {
        public int volumeCreate(decimal reqid,int appid ,string volume,int category,long userid,string token, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("volumeDetailCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd,reqid,appid, userid, token);
                    cmd.Parameters.Add("@Volume", SqlDbType.VarChar, 100).Value = volume;                   
                    cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = category;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        public DataTable volumeReturn(decimal reqid, int appid, string token, int VolumeID, int CategoryID,int TypeID, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("volumeReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid, token);
                    cmd.Parameters.Add("@VolumeID", SqlDbType.Int).Value = VolumeID;
                    cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = CategoryID;
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = TypeID;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
    }
}
