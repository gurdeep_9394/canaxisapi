﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface.ICommon
{
    public interface INotificationID
    {
        long NotificationID { get; set; }
    }
    public interface INotificationCount
    {
        int NotificationCount { get; set; }
    }
    public interface INotificationText
    {
        string NotificationText { get; set; }
    }
    public interface INotificationType
    {
        string NotificationType { get; set; }
    }
}
