﻿using API.Repo.Helper;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using API.Core.Models.Common;
using API.Core.Models.Authentication;
using System.Collections.Generic;
using System.Data;
using API.Core.ViewModel.User;
using System.IO;

namespace ProjectX.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class OTPController : ApiController
    {/// <summary>
    /// 
    /// </summary>
    /// <param name="OperandID"></param>
    /// <param name="contactno"></param>
    /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Create(long OperandID,long contactno)
        {
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }
               
                string message = "";

                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();

                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                UserAccountPasskey _objUsers = new UserAccountPasskey();
                string OTP = ua.OTP();

                if (ua.userOTPCreate(reqid, appid, OperandID, OTP,1, userid, out message) == 1)
                {
                    string OTPMessage = "Your One Time Password is: " + OTP + " to proceed on MrFreshEats. It is Valid for 30 minutes. Thank you for choosing us.\n\nMrFreshEats";
                    ua.SendSMS(contactno, OTPMessage);
                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="OperandID"></param>
        /// <param name="OTP"></param>
        /// <param name="Contact"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Verify(long OperandID, string OTP, long Contact)
        {
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

               
                string message = "";

                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();

                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                UserAccountPasskey _objUsers = new UserAccountPasskey();


                if (ua.userContactVerify(reqid, appid, OperandID, OTP, Contact, userid, out message) == 1)
                {
                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Contact"></param>
        /// <returns></returns>
        #region Return
        [HttpGet]
        public IHttpActionResult Return(long Contact)
        {
            OTPViewModel user = new OTPViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = 957850;
                long userid = 1371352;
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();
                int stat = -1;
                string msg = "";
                List<OTPView> Detail = new List<OTPView>();
                DataTable detail = ua.OTPReturn(reqid, appid, Contact, userid, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new OTPView
                                  {
                                      Basic = new OTPDetail()
                                      {
                                          OTP = item.Field<string>("OTP"),
                                      },
                                     
                                  }).ToList();
                        user.DetailObject = Detail;
                        user.Message = "OTP return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.DetailObject = null;
                        user.Message = "No OTP is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.DetailObject = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.DetailObject = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
        #endregion

        
    }
}
