﻿using API.Core.ViewModel.User;
using API.Repo.Helper;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using API.Core.Models.Common;
using API.Core.Models.Authentication;
using ProjectX.Repository.User;
using System.Data;
using API.Core.ViewModel.Authentication;
using System.Text;
using System.Collections;
using API.Core.Models.User;
using API.Repo.User;

namespace ProjectX.WebAPI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class GuestUserController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Create(UserProfile user)
        {
            UserLoginViewModel currentUser = new UserLoginViewModel();
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }
                string message = "";
                int stat = -1;
                string msg = "";

                UserLoginAuthViewModel Login = new UserLoginAuthViewModel();
                UserAccountPasskey Detail = new UserAccountPasskey();
                UserLoginView DetailType = new UserLoginView();
                DataTable dtlogin = new DataTable();

                API.Repo.User.UserAccount ua = new API.Repo.User.UserAccount();
                userid = ua.userUniqueID();
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                UserAccountPasskey _objUsers = new UserAccountPasskey();
                EncryptHelperObj _objuserEncryptHelperObj = new EncryptHelperObj();
                _objuserEncryptHelperObj = EncryptHelper.Get_EncryptedPassword(_objuserEncryptHelperObj, user.Password);
                _objUsers.Passkey = _objuserEncryptHelperObj.Value;
                _objUsers.SaltKey = _objuserEncryptHelperObj.SaltKey;
                _objUsers.SaltKeyIV = _objuserEncryptHelperObj.SaltKeyIV;
                userAuthentication sa = new userAuthentication();


                 dtlogin = sa.userLoginMobileID(reqid, appid, user.MobileDetail, out stat, out msg);
                Login.Message = msg;
                Login.Status = stat;
                if (stat == 1)
                {
                    if (dtlogin != null)
                    {
                        Detail = (from item in dtlogin.AsEnumerable()
                                  select new UserAccountPasskey
                                  {
                                      SaltKey = item.Field<string>("SaltKey"),
                                      SaltKeyIV = item.Field<string>("SaltKeyIV"),
                                      Passkey = item.Field<string>("Passkey"),
                                  }).FirstOrDefault();
                        DetailType = (from item in dtlogin.AsEnumerable()
                                      select new UserLoginView
                                      {
                                          Type = new UserType
                                          {
                                              UserTypeID = item.Field<int>("TypeID"),
                                              UserTypeName = item.Field<string>("TypeName"),

                                          },
                                          UserID = item.Field<long>("UserID")
                                      }).FirstOrDefault();

                        //EncryptHelper eh = new EncryptHelper();
                        EncryptHelper eh = new EncryptHelper();
                        byte[] encPas = eh.EncryptString_New(Detail.Passkey, Encoding.Default.GetBytes(Detail.SaltKey), Encoding.Default.GetBytes(Detail.SaltKeyIV));
                        byte[] pas = Encoding.Default.GetBytes(Detail.Passkey);

                        userid = DetailType.UserID;

                       // if (StructuralComparisons.StructuralEqualityComparer.Equals(encPas, pas))
                        //{
                            Login.Login = DetailType;
                            string tokenid = "";
                            int check = 0;
                            do
                            {
                                tokenid = Guid.NewGuid().ToString();
                                tokenid = tokenid.Replace("-", "");
                                check = sa.userAuthenticationTokenCreate(reqid, tokenid, userid, out msg);
                                if (msg != "Token already exists")
                                {
                                    currentUser.Message = msg;
                                    break;
                                }
                            }
                            while (check != 1);
                            if (check == 0)
                            {
                                currentUser.Detail = DetailType.Type;
                                currentUser.Token = "NA";
                                currentUser.UserID = userid;
                                currentUser.Message = currentUser.Message;
                                currentUser.Status = check;
                            }
                            else
                            {
                                currentUser.Detail = DetailType.Type;
                                currentUser.Token = tokenid;
                                currentUser.UserID = userid;
                                currentUser.Message = "User logged in successfully!";
                                currentUser.Status = 1;
                            }
                      //  }
                        //else
                        //{
                        //    currentUser.Detail = null;
                        //    currentUser.Token = "";
                        //    currentUser.UserID = -1;
                        //    currentUser.Message = "Username or Password does not match. Please try again.";
                        //    currentUser.Status = 0;
                        //}
                    }
                    else
                    {
                        currentUser.Detail = null;
                        currentUser.Token = "";
                        currentUser.UserID = -1;
                        currentUser.Message = "Username or Password does not match. Please try again.";
                        currentUser.Status = 0;
                    }

                    sr.requestDetailAdd(reqid, "Post", Login.Status, "Authentication", "userAccountPasskey", "Login", Convert.ToString(user.ContactNo) + ": " + Login.Message, userid, userid, out msg);

                    return Ok(currentUser);
                }
                else
                {
                    if (ua.userAccountCreate( userid, user.UserName, user.FullName, user.AddressLine, user.ZipCode, user.CityID, user.ContactNo, user.EmailID, _objUsers.Passkey, _objUsers.SaltKey, _objUsers.SaltKeyIV, user.UserTypeID, user.MobileDetail, user.Image, userid, out message) == 1)
                    {
                        //result.Status = 1;

                        //result.Message = message;

                        //USerLogin
                        dtlogin = sa.userLoginUserID(reqid, appid, userid, out stat, out msg);
                        Login.Message = msg;
                        Login.Status = stat;
                        if (stat == 1)
                        {
                            if (dtlogin != null)
                            {
                                Detail = (from item in dtlogin.AsEnumerable()
                                          select new UserAccountPasskey
                                          {
                                              SaltKey = item.Field<string>("SaltKey"),
                                              SaltKeyIV = item.Field<string>("SaltKeyIV"),
                                              Passkey = item.Field<string>("Passkey"),
                                          }).FirstOrDefault();
                                DetailType = (from item in dtlogin.AsEnumerable()
                                              select new UserLoginView
                                              {
                                                  Type = new UserType
                                                  {
                                                      UserTypeID = item.Field<int>("TypeID"),
                                                      UserTypeName = item.Field<string>("TypeName"),

                                                  },
                                                  UserID = item.Field<long>("UserID")
                                              }).FirstOrDefault();

                                //EncryptHelper eh = new EncryptHelper();
                                EncryptHelper eh = new EncryptHelper();
                                byte[] encPas = eh.EncryptString_New(user.Password, Encoding.Default.GetBytes(Detail.SaltKey), Encoding.Default.GetBytes(Detail.SaltKeyIV));
                                byte[] pas = Encoding.Default.GetBytes(Detail.Passkey);

                                userid = DetailType.UserID;

                                if (StructuralComparisons.StructuralEqualityComparer.Equals(encPas, pas))
                                {
                                    Login.Login = DetailType;
                                    string tokenid = "";
                                    int check = 0;
                                    do
                                    {
                                        tokenid = Guid.NewGuid().ToString();
                                        tokenid = tokenid.Replace("-", "");
                                        check = sa.userAuthenticationTokenCreate(reqid, tokenid, userid, out msg);
                                        if (msg != "Token already exists")
                                        {
                                            currentUser.Message = msg;
                                            break;
                                        }
                                    }
                                    while (check != 1);
                                    if (check == 0)
                                    {
                                        currentUser.Detail = DetailType.Type;
                                        currentUser.Token = "NA";
                                        currentUser.UserID = userid;
                                        currentUser.Message = currentUser.Message;
                                        currentUser.Status = check;
                                    }
                                    else
                                    {
                                        currentUser.Detail = DetailType.Type;
                                        currentUser.Token = tokenid;
                                        currentUser.UserID = userid;
                                        currentUser.Message = "User logged in successfully!";
                                        currentUser.Status = 1;
                                    }
                                }
                                else
                                {
                                    currentUser.Detail = null;
                                    currentUser.Token = "";
                                    currentUser.UserID = -1;
                                    currentUser.Message = "Username or Password does not match. Please try again.";
                                    currentUser.Status = 0;
                                }
                            }
                            else
                            {
                                currentUser.Detail = null;
                                currentUser.Token = "";
                                currentUser.UserID = -1;
                                currentUser.Message = "Username or Password does not match. Please try again.";
                                currentUser.Status = 0;
                            }

                            sr.requestDetailAdd(reqid, "Post", Login.Status, "Authentication", "userAccountPasskey", "Login", Convert.ToString(user.ContactNo) + ": " + Login.Message, userid, userid, out msg);

                            return Ok(currentUser);
                        }
                        else
                        {
                            currentUser.Detail = null;
                            currentUser.Token = "";
                            currentUser.UserID = -1;
                            currentUser.Message = msg;
                            currentUser.Status = stat;

                            return Ok(currentUser);
                        }

                        //end user login
                        // return Content(HttpStatusCode.OK, result);

                    }
                    else
                    {
                        result.Status = 0;
                        result.Message = message;
                        return Ok(result);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }
    }
}
