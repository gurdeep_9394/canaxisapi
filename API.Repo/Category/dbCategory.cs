﻿using API.Repo.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;


namespace API.Repo.Category
{
    public class dbCategory
    {
        public DataTable testCategoryDetail(decimal reqid, int appid, string token, int TypeID,int categoryID, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("testCategoryDetail", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid, token);
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = TypeID;
                    cmd.Parameters.Add("@CategoryID", SqlDbType.Int).Value = categoryID;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        public int testCategoryCreate(decimal reqid, int appid, string category, int typeid ,long userid, string token, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("testCategoryCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid, token);
                    cmd.Parameters.Add("@CategoryName", SqlDbType.VarChar, 100).Value = category;
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = typeid;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
    }
}
