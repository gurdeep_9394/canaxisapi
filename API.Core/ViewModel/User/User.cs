﻿using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;
using API.Core.Models.Authentication;
using API.Core.Models.Common;
using API.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.ViewModel.User
{
    public class UserLoginView : IUserID
    {
        public UserType Type { get; set; }

        public long UserID { get; set; }

    }

    public class UserLoginDetailViewModel : IResult
    {
        public UserLoginDetail Login { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }
    }

    public class UserDetailView
    {
        public UserProfile Basic { get; set; }
    }
    public class UserDetailViewModel: IStatusID, IMessage
    {
        public List<UserDetailView> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }

    }
    public class WalletView
    {
        public WalletReturn Basic { get; set; }
    }
    public class WalletViewModel : IStatusID, IMessage
    {
        public List<WalletView> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }

    }
    public class TransactionView
    {
        //public TransactionReturn Basic { get; set; }
    }
    public class TransactionViewModel : IStatusID, IMessage
    {
        public List<TransactionView> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }

    }
    public class AppVersionDetail : IAppVersion
    {
        public string AppVersion { get; set; }
    }

    public class AppVersionViewModel : IStatusID, IMessage
    {
        public List<AppVersionDetail> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }

    }

}
