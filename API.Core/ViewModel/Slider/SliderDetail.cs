﻿using API.Core.Interface.ICommon;
using API.Core.Models.Common;
using System.Collections.Generic;

namespace API.Core.ViewModel.Slider
{

    public class SliderDetailView : IActive
    {
        public SliderDetail Basic { get; set; }
        public int isActive { get; set; }
    }
    public class SliderDetailViewModel : IStatusID, IMessage
    {
        public List<SliderDetailView> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
