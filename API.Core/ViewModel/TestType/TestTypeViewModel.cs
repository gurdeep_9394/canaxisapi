﻿using API.Core.Interface.ICommon;
using API.Core.Models.TestType;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.ViewModel.TestType
{
    public class TestTypeViewModel : IResult       
    {
        public List<TestTypeModel> Details { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
