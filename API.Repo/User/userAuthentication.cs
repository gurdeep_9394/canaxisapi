﻿using API.Repo.Helper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web;

namespace API.Repo.User
{
    public class userAuthentication
    {
        #region User Login

        public DataTable userLoginContact(long contact, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userLoginContact", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                   
                    cmd.Parameters.Add("@Contact", SqlDbType.BigInt).Value = contact;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public DataTable userLoginUserID(decimal reqid, int appid, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userLoginUserID", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        public DataTable userLoginMobileID(decimal reqid, int appid, string mobileid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userLoginMobileID", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@MobileID", SqlDbType.VarChar,200).Value = mobileid;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        
        #endregion

        #region User Password Reset

        public int UserAccountPasskeyReset(decimal reqid, int appid, long operand, int typeid, string passkey, string saltkey, string saltkeyiv, long userid, string token, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAccountPasskeyReset", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd,reqid,appid, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = typeid;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@Saltkey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltkeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int userPasskeyReset(decimal reqid, int appid, long OperandID, string passkey, string saltkey, string saltkeyiv, string oldpasskey, string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userPasskeyReset", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@OldPasskey", SqlDbType.VarChar, 500).Value = oldpasskey;
                    cmd.Parameters.Add("@SaltKey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltKeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public DataTable userForgetPasskey(decimal reqid, int appid, long ContactNo, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userForgetPasskey", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid);
                    cmd.Parameters.Add("@ContactNo", SqlDbType.BigInt).Value = ContactNo;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public int userForgetPasskeyReset(decimal reqid, int appid, long OperandID, string OTP, string passkey, string saltkey, string saltkeyiv, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userForgetPasskeyReset", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@OTP", SqlDbType.VarChar, 500).Value = OTP;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@SaltKey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltKeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        public int userSignOut(decimal reqid, int appid, long OperandID, string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userSignOut", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid,token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = OperandID;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region User Password Update

        public int UserAccountPasskeyUpdate(decimal reqid, int appid, long operand, string newpasskey, string newsaltkey, string newsaltkeyiv, string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAccountPasskeyUpdate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, reqid, appid, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@NewPasskey", SqlDbType.VarChar, 500).Value = newpasskey;
                    cmd.Parameters.Add("@NewSaltkey", SqlDbType.VarChar, 500).Value = newsaltkey;
                    cmd.Parameters.Add("@NewSaltkeyIV", SqlDbType.VarChar, 500).Value = newsaltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        
        #endregion

        #region Password Return

        public DataTable userPasskeyReturn(decimal reqid, int appid, string token, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userPasskeyReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd,reqid, appid, userid, token);
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        #endregion

        #region Token

        public int userAuthenticationTokenCreate(decimal reqid, string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAuthenticationTokenCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                RequestHelper sr = new RequestHelper();
                sr.requestFailAdd(reqid, "userAuthenticationTokenCreate", msg, userid, out msg);
                return 0;
            }
            finally
            {

            }
        }

        public int userAuthenticationTokenDelete(decimal reqid, int appid, int appiddelete, string token, long operand, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAuthenticationTokenDelete", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@AppIDDelete", SqlDbType.Int).Value = appiddelete;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region OTP

        public string OTP()
        {
            try
            {

                string ss = "00";
                int stat = 99;
                do
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    decimal numIterations = 0;
                    numIterations = rand.Next(1111, 9999);
                    ss = numIterations.ToString();
                    stat = 1;
                }
                while (stat != 1);

                if (stat == 1)
                {

                    return ss;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                return "0";
            }
            finally
            {

            }
        }

        #endregion

        #region OTP
        public int userOTPCreate(decimal reqid, int appid, long OperandID, string OTP, int isvalid, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userOTPCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd,reqid,appid, userid);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@OTP", SqlDbType.VarChar, 200).Value = OTP;
                    cmd.Parameters.Add("@isValid", SqlDbType.Int).Value = isvalid;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int SendSMS(long Contact, string Message)
        {
            try
            {
                WebRequest request = HttpWebRequest.Create("http://101.smssansaar.com/api/swsendSingle.asp?username=t2mrfresheats&password=rj13g3193&sender=MFEATS&sendto=" + Contact + "&message=" + Message);
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string urlText = reader.ReadToEnd();
                if (urlText != "")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }


        }

        #endregion
    }
}
