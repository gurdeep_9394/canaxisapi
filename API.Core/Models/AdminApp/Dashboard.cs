﻿using System;
using API.Core.Interface.AdminApp;

namespace API.Core.Models.AdminApp
{
    public class DashboardCount : ITotalOrders,IPendingOrders,IProcessingOrders,ICompletedOrders,ITotalReturns,IPendingReturns,IProcessingReturns,ICompletedReturns,IAcceptedOrders,IInTransitOrders,IAcceptedReturns,IInTransitReturns
    {
        public int TotalOrders { get; set; }
        public int PendingOrders { get; set; }
        public int ProcessingOrders { get; set; }
        public int CompletedOrders { get; set; }
        public int TotalReturns { get; set; }
        public int PendingReturns { get; set; }
        public int ProcessingReturns { get; set; }
        public int CompletedReturns { get; set; }

        public int AcceptedOrders { get; set; }

        public int InTransitOrders { get; set; }

        public int AcceptedReturns { get; set; }

        public int InTransitReturns { get; set; }
    }
}
