﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace API.Repo.Helper
{
    public class RequestHelper
    {
        #region Constructor

        public RequestHelper()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion

        #region UniqueID

        public decimal requestUniqueID(long userid)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                RequestID = 0;
                string ss = "00";
                int stat = 99;
                string eror = "empty";
                do
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    decimal numIterations = 0;
                    numIterations = rand.Next(11111111, 99999999);
                    if (userid != -1)
                    {
                        ss = userid.ToString() + numIterations.ToString();
                    }
                    else
                    {
                        ss = numIterations.ToString();
                    }

                    using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand("dbackerRequestUniqueID", connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = ss;
                        SqlParameter parm = new SqlParameter("@Stat", SqlDbType.Int);
                        parm.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(parm);
                        SqlParameter parm2 = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
                        parm2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(parm2);
                        cmd.ExecuteNonQuery();
                        stat = Convert.ToInt16(parm.Value);
                        eror = parm2.Value.ToString();
                    }
                }
                while (stat != 1);

                if (stat == 1)
                {
                    RequestID = Convert.ToDecimal(ss);
                    return RequestID;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region Function

        #region Common

        public int requestLogAdd(decimal reqid, int appid, string localAddress, string remoteAddress, string remoteHost, string macAddress, string browser, string city, string country, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("dbackerRequestLogAdd", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@LocalAddress", SqlDbType.VarChar, 20).Value = localAddress;
                    cmd.Parameters.Add("@RemoteAddress", SqlDbType.VarChar, 20).Value = remoteAddress;
                    cmd.Parameters.Add("@RemoteHost", SqlDbType.VarChar, 50).Value = remoteHost;
                    cmd.Parameters.Add("@PhysicalAddress", SqlDbType.VarChar, 50).Value = macAddress;
                    cmd.Parameters.Add("@UserAgent", SqlDbType.VarChar, 512).Value = browser;
                    cmd.Parameters.Add("@UserCountry", SqlDbType.VarChar, 50).Value = country;
                    cmd.Parameters.Add("@UserCity", SqlDbType.VarChar, 50).Value = city;
                    cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;

                    SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
                    stat.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(stat);
                    SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
                    message.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(message);
                    cmd.ExecuteNonQuery();
                    msg = message.Value.ToString();
                    return Convert.ToInt16(stat.Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int requestLogAdd(long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("dbackerRequestLogAdd", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = RequestID;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = Convert.ToInt32(1234);
                    cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int requestDetailAdd(decimal reqid, string typeid, int status, string module, string table, string operation, string detail, long operand, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("dbackerRequestDetailAdd", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@Type", SqlDbType.VarChar, 20).Value = typeid;
                    cmd.Parameters.Add("@Status", SqlDbType.Int).Value = status;
                    cmd.Parameters.Add("@Module", SqlDbType.VarChar, 50).Value = module;
                    cmd.Parameters.Add("@TableName", SqlDbType.VarChar, 50).Value = table;
                    cmd.Parameters.Add("@Operation", SqlDbType.VarChar, 50).Value = operation;
                    cmd.Parameters.Add("@Detail", SqlDbType.VarChar, 1024).Value = detail;
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;

                    SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
                    stat.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(stat);
                    SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
                    message.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(message);
                    cmd.ExecuteNonQuery();
                    msg = message.Value.ToString();
                    return Convert.ToInt16(stat.Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int requestValidate(decimal reqid, int appid, string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("dbackerRequestValidate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);

                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int requestFailAdd(decimal reqid, string page, string msg, long userid, out string error)
        {
            requestLogAdd(userid, out error);
            error = "An error occured while processing your request. Please try again in some time." + "<br/>Error ID: " + reqid;
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("dbackerRequestFailAdd", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@ProcedureName", SqlDbType.VarChar, 100).Value = page;
                    cmd.Parameters.Add("@Message", SqlDbType.VarChar, 256).Value = msg;
                    cmd.ExecuteNonQuery();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                error = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        public DataTable requestDetailReturn(decimal reqid, int appid, string module, string requesttype, int requeststatus, string operation, DateTime date, long operand, int isactive, int isdeleted, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("dbackerRequestDetailReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@Module", SqlDbType.VarChar, 50).Value = module;
                    cmd.Parameters.Add("@RequestType", SqlDbType.VarChar, 50).Value = requesttype;
                    cmd.Parameters.Add("@RequestStatus", SqlDbType.Int).Value = requeststatus;
                    cmd.Parameters.Add("@Operation", SqlDbType.VarChar, 50).Value = operation;
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@Date", SqlDbType.Date).Value = date;
                    cmd.Parameters.Add("@isActive", SqlDbType.Int).Value = isactive;
                    cmd.Parameters.Add("@isDeleted", SqlDbType.Int).Value = isdeleted;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        #endregion

        #region Property

        public static decimal RequestID { get; set; }
        public static string LocalAddress { get; set; }
        public static string RemoteAddress { get; set; }
        public static string RemoteHost { get; set; }
        public static string PhysicalAddress { get; set; }
        public static string UserAgent { get; set; }
        public static string UserCountry { get; set; }
        public static string UserCity { get; set; }

        #endregion
    }
}
