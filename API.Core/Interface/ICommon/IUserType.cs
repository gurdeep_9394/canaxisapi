﻿namespace API.Core.Interface.ICommon
{
    public interface IUserTypeID
    {
        int UserTypeID { get; set; }
    }

    public interface IUserTypeName
    {
        string UserTypeName { get; set; }
    }
}
