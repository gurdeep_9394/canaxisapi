﻿using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;

namespace API.Core.Models.User
{
    public class UserContact:IContact, IIsPrimary
    {
        public long ContactNo { get; set; }

        public int isPrimary { get; set; }

    }

    public class UserEmail : IEmail, IIsPrimary
    {
        public string EmailID { get; set; }

        public int isPrimary { get; set; }

    }
}
