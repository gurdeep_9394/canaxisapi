﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface.ICommon
{
    interface IImageID
    {
        int ImageID { get; set; }
    }
    interface IImageName
    {
        string ImageName { get; set; }
    }
    interface IPriorityID
    {
        int PriorityID { get; set; }
    }

}
