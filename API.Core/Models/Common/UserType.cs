﻿using API.Core.Interface.ICommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Models.Common
{
    public class UserType : IUserTypeID, IUserTypeName
    {
        public int UserTypeID { get; set; }

        public string UserTypeName { get; set; }
    }
}
