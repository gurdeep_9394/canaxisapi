﻿
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace API.Repo.Helper
{
    public class DatabaseHelper
    {
        #region Constructor

        public DatabaseHelper()
        {
           // ConnectionString = "Data Source= DESKTOP-HLB57U4\\SNINGERUSER;User Id=sningerUser; Password=rj13g3193 ;   Initial Catalog=CanaxisDB;Persist Security Info=True;MultipleActiveResultSets=True;Min Pool Size=20;Asynchronous Processing=true;";
            ConnectionString = "Data Source= 148.72.232.167;User Id=canaxis; Password=Singh@123 ;   Initial Catalog=CanaxisDB;Persist Security Info=True;MultipleActiveResultSets=True;Min Pool Size=20;Asynchronous Processing=true;";
        }

        #endregion

        #region Common Functions

        public System.Data.DataTable ReturnTable(string cmd, SqlConnection con, out int status, out string message)
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd, con);
                da.Fill(ds);
                if (ds.Tables.Count != 0)
                {
                    dt = ds.Tables[0]; 
                    if (dt.Rows.Count != 0)
                    {
                        status = 1;
                        message = "Data loaded successfully!";
                        return dt;
                    }
                    else
                    {
                        status = 1;
                        message = "No data found to load!";
                        dt = null;
                        return dt;
                    }
                }
                else
                {
                    status = 1;
                    message = "No data found to load!";
                    return null;
                }
            }
            catch (Exception ex)
            {
                status = 0;
                message = ex.Message.ToString(); ;
                dt = null;
                return dt;
            }
        }

        public DataTable ReturnTable(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables.Count != 0)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count != 0)
                    {
                        return dt;
                    }
                    else
                    {
                        dt = null;
                        return dt;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = ex.Message.ToString();
                cmd.Parameters["@Stat"].Value = 0;
                cmd.Parameters["@Message"].Value = ex.Message.ToString();
                dt = null;
                return dt;
            }
        }

        public SqlCommand ReturnBasic(SqlCommand cmd, long userid, string token)
        {
            cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
            cmd.Parameters.Add("@Token", SqlDbType.VarChar, 50).Value = token;

            SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
            stat.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(stat);
            SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
            message.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(message);
            return cmd;
        }
        public SqlCommand ReturnBasic(SqlCommand cmd, long userid)
        {
            cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;

            SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
            stat.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(stat);
            SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
            message.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(message);
            return cmd;
        }
        public SqlCommand ReturnBasic(SqlCommand cmd, decimal reqid, int appid, long userid)
        {
            cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
            cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
            cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
            cmd.Parameters.Add("@LocalAddress", SqlDbType.VarChar, 20).Value = "LOCAL_ADDR";
            cmd.Parameters.Add("@RemoteAddress", SqlDbType.VarChar, 20).Value = "REMOTE_ADDR";
            cmd.Parameters.Add("@RemoteHost", SqlDbType.VarChar, 50).Value = "REMOTE_HOST";
            cmd.Parameters.Add("@PhysicalAddress", SqlDbType.VarChar, 50).Value = "Phycical Address";
            cmd.Parameters.Add("@UserAgent", SqlDbType.VarChar, 512).Value = "HTTP_USER_AGENT";
            cmd.Parameters.Add("@UserCountry", SqlDbType.VarChar, 50).Value = "India";
            cmd.Parameters.Add("@UserCity", SqlDbType.VarChar, 50).Value = "Chandigarh";

            SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
            stat.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(stat);
            SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
            message.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(message);
            return cmd;
        }

        public SqlCommand ReturnBasic(SqlCommand cmd, decimal reqid, int appid, long userid,string token)
        {
            cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
            cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
            cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
            cmd.Parameters.Add("@Token", SqlDbType.VarChar, 50).Value = token;
            cmd.Parameters.Add("@LocalAddress", SqlDbType.VarChar, 20).Value = "LOCAL_ADDR";
            cmd.Parameters.Add("@RemoteAddress", SqlDbType.VarChar, 20).Value = "REMOTE_ADDR";
            cmd.Parameters.Add("@RemoteHost", SqlDbType.VarChar, 50).Value = "REMOTE_HOST";
            cmd.Parameters.Add("@PhysicalAddress", SqlDbType.VarChar, 50).Value = "Phycical Address";
            cmd.Parameters.Add("@UserAgent", SqlDbType.VarChar, 512).Value = "HTTP_USER_AGENT";
            cmd.Parameters.Add("@UserCountry", SqlDbType.VarChar, 50).Value = "India";
            cmd.Parameters.Add("@UserCity", SqlDbType.VarChar, 50).Value = "Chandigarh";

            SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
            stat.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(stat);
            SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
            message.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(message);
            return cmd;
        }

        public SqlCommand ReturnBasic(SqlCommand cmd)
        {

            SqlParameter stat = new SqlParameter("@Stat", SqlDbType.Int);
            stat.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(stat);
            SqlParameter message = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
            message.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(message);
            return cmd;
        }

        #endregion

        #region Properties

        public string Message
        {
            get;
            set;
        }

        public string Result
        {
            get;
            set;
        }

        public string ConnectionString
        {
            get;
            set;
        }

        #endregion
    }
}
