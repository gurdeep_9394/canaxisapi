﻿
using API.Repo.Helper;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ProjectX.Repository.Slider
{
   public class Slider
    {
        #region sliderReturn
        public DataTable sliderImageReturn(decimal reqid, int appid, string token, int isactive, int isdeleted, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("sliderImageReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@isActive", SqlDbType.Int).Value = isactive;
                    cmd.Parameters.Add("@isDeleted", SqlDbType.Int).Value = isdeleted;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        #endregion
    }
}
