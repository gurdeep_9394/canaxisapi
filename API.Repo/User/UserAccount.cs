﻿using API.Repo.Helper;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;

namespace API.Repo.User
{
    public class UserAccount
    {
        #region Unique ID
        public long userUniqueID()
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                long userid = 0;
                string ss = "00";
                int stat = 99;
                string eror = "empty";
                do
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    long numIterations = 0;
                    numIterations = rand.Next(100000, 999999);
                    ss = "4815" + numIterations.ToString();
                    using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                    {
                        connection.Open();
                        SqlCommand cmd = new SqlCommand("userUniqueID", connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        cmd.Parameters.Add("@ID", SqlDbType.BigInt).Value = ss;
                        SqlParameter parm = new SqlParameter("@Stat", SqlDbType.Int);
                        parm.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(parm);
                        SqlParameter parm2 = new SqlParameter("@Message", SqlDbType.VarChar, 1024);
                        parm2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(parm2);
                        cmd.ExecuteNonQuery();
                        stat = Convert.ToInt16(parm.Value);
                        eror = parm2.Value.ToString();
                    }
                }
                while (stat != 1);

                if (stat == 1)
                {
                    userid = Convert.ToInt64(ss);
                    return userid;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {

            }
        }
        #endregion

        #region OTP

        public string OTP()
        {
            try
            {
                
                string ss = "00";
                int stat = 99;
                do
                {
                    Random rand = new Random((int)DateTime.Now.Ticks);
                    decimal numIterations = 0;
                    numIterations = rand.Next(1111, 9999);
                    ss = numIterations.ToString();
                    stat = 1;
                }
                while (stat != 1);

                if (stat == 1)
                {

                    return ss;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception ex)
            {
                return "0";
            }
            finally
            {

            }
        }

        #endregion

        #region UserProfileRetun
        public DataTable userProfileReturn(decimal reqid, int appid, long operand, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userProfileReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = operand;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public int userProfileEdit(decimal reqid, int appid,string Image, string UserName, long userid, string token,out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userProfileEdit", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid,token);
                    cmd.Parameters.Add("@UserName", SqlDbType.VarChar,20).Value = UserName;
                    cmd.Parameters.Add("@Image", SqlDbType.VarChar, 200).Value = Image;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        #endregion

        #region ProfileCreate

       
        public int userAccountCreate( long operand, string username, string fullname, string address, string ZipCode, int cityid, long contact, string email, string passkey, string saltkey, string saltkeyiv, int typeid, string mobileDetail,string Image ,long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAccountCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd,userid);
                    cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Value = username;
                    cmd.Parameters.Add("@FullName", SqlDbType.VarChar, 100).Value = fullname;
                    cmd.Parameters.Add("@Address", SqlDbType.VarChar, 200).Value = address;
                    cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar, 100).Value = ZipCode;
                    cmd.Parameters.Add("@CityID", SqlDbType.Int).Value = cityid;
                    cmd.Parameters.Add("@ContactNo", SqlDbType.BigInt).Value = contact;
                    cmd.Parameters.Add("@EmailID", SqlDbType.VarChar, 100).Value = email;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@SaltKey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltKeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.Parameters.Add("@MobileDetail", SqlDbType.VarChar, 100).Value = mobileDetail;
                    cmd.Parameters.Add("@Image", SqlDbType.VarChar, 1024).Value = Image;
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = typeid;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
       

        public int userPasskeyReset(decimal reqid, int appid, long OperandID, string passkey, string saltkey,string saltkeyiv,string oldpasskey,string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userPasskeyReset", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@OldPasskey", SqlDbType.VarChar, 500).Value = oldpasskey;
                    cmd.Parameters.Add("@SaltKey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltKeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public DataTable userForgetPasskey(decimal reqid, int appid, long ContactNo, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userForgetPasskey", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@ContactNo", SqlDbType.BigInt).Value = ContactNo;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public int userForgetPasskeyReset(decimal reqid, int appid, long OperandID,string OTP, string passkey, string saltkey, string saltkeyiv, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userForgetPasskeyReset", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@OTP", SqlDbType.VarChar, 500).Value = OTP;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@SaltKey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltKeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        public int userSignOut(decimal reqid, int appid, long OperandID,string token, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userSignOut", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid,token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = OperandID;                   
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        #endregion

        #region Address
        public int userAddressCreate(decimal reqid, int appid,  string AddressLine, int CityID,string Lat, string Lng, string detail , long zipCode, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAddressCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = userid;
                    cmd.Parameters.Add("@AddressLine", SqlDbType.VarChar, 250).Value = AddressLine;
                    cmd.Parameters.Add("@CityID", SqlDbType.Int).Value = CityID;
                    cmd.Parameters.Add("@ZipCode", SqlDbType.BigInt).Value = zipCode;
                    cmd.Parameters.Add("@Lat", SqlDbType.VarChar, 50).Value = Lat;
                    cmd.Parameters.Add("@Lng", SqlDbType.VarChar, 50).Value = Lng;
                    cmd.Parameters.Add("@Detail", SqlDbType.VarChar, 500).Value = detail;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion
        #region contact
        public int userContactCreate(decimal reqid, int appid, long OperandID, long Contact, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userContactCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@ContactNo", SqlDbType.BigInt).Value = Contact;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }
        public int userContactUpdate(decimal reqid, int appid, long OperandID, long Contact,string OTP, long userid,string token, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userContactUpdate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid,token);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@ContactNo", SqlDbType.BigInt).Value = Contact;
                    cmd.Parameters.Add("@OTP", SqlDbType.VarChar,20).Value = OTP;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region OTP
        public int userOTPCreate(decimal reqid, int appid, long OperandID, string OTP, int isvalid, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userOTPCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@OTP", SqlDbType.VarChar, 200).Value = OTP;
                    cmd.Parameters.Add("@isValid", SqlDbType.Int).Value = isvalid;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        public int SendSMS(long Contact, string Message)
        {
            try
            {
                WebRequest request = HttpWebRequest.Create("http://101.smssansaar.com/api/swsendSingle.asp?username=t2mrfresheats&password=rj13g3193&sender=MFEATS&sendto=" + Contact + "&message=" + Message);
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string urlText = reader.ReadToEnd();
                if (urlText != "")
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            
        }

        #endregion

        #region Contact varify
        public int userContactVerify(decimal reqid, int appid, long OperandID, string OTP, long contact, long userid, out string msg)
        {
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userContactVerify", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@OperandID", SqlDbType.BigInt).Value = OperandID;
                    cmd.Parameters.Add("@OTP", SqlDbType.VarChar, 200).Value = OTP;
                    cmd.Parameters.Add("@Contact", SqlDbType.BigInt).Value = contact;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region UserContact Return
        public DataTable userContactByOrderID(decimal reqid, int appid, int orderid, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userContactByOrderID", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@OrderID", SqlDbType.Int).Value = orderid;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        #endregion

        public DataTable OTPReturn(decimal reqid, int appid, long contact, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("OTPReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid);
                    cmd.Parameters.Add("@Contact", SqlDbType.BigInt).Value = contact;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public DataTable userTypeReturn(decimal reqid, int appid, int id,int active, int deleted, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userTypeReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@isActive", SqlDbType.Int).Value = active;
                    cmd.Parameters.Add("@isDeleted", SqlDbType.Int).Value = deleted;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public DataTable appVersionReturn(decimal reqid, int appid, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("appVersionReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public DataTable appVersionAdminReturn(decimal reqid, int appid, int id, long userid, string token, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            DatabaseHelper dbObject = new DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("appVersionAdminReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = id;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
    }

}
