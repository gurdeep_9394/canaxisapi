﻿using API.Core.Interface.ICommon;
using API.Core.Models.Category;
using API.Core.Models.TestType;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.ViewModel.Category
{
    public class CategoryViewModel : IResult
    {
        public List<CategoryModel> Details { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
