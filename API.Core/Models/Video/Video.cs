﻿using sh.Core.Interface.IVideo;
using System;
using System.Collections.Generic;
using System.Text;

namespace sh.Core.Models.Video
{
    public class VideoModel : IVideo
    {
        public string VideoDetail { get; set; }
        public int VideoID { get; set; }
        public string VideoLink { get; set; }
        public string VideoName { get; set; }
    }
}
