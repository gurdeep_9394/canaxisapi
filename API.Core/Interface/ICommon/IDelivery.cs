﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface.ICommon
{
    interface IDeliveryTypeID
    {
        int DeliveryTypeID { get; set; }
    }
    interface IDeliveryTypeName
    {
        string DeliveryTypeName { get; set; }
    }
    public interface IMinValue
    {
        decimal MinValue { get; set; }
    }
}
