﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.ICategory
{
   public interface ICategory: ICategoryID, ICategoryName
    {
    }
    public interface ICategoryID
    {
        int CategoryID { get; set; }
    }
    public interface ICategoryName 
    {
        string CategoryName { get; set; }
    }
}
