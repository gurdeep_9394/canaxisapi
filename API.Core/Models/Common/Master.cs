﻿using API.Core.Interface.ICommon;
using API.Core.Interface.IUser;
using System;

namespace API.Core.Models.Common
{
    public class App : IApp
    {
        public int AppID { get; set; }
    }

    public class Result : IResult
    {
        public int Status { get; set; }

        public string Message { get; set; }
    }

    public class Status : IRecordStatus
    {
        public int isActive { get; set; }

        public int isDeleted { get; set; }
    }

    #region Address Components

    #region Country

    public class Country : ICountryID, ICountryName
    {
        public int CountryID { get; set; }

        public string CountryName { get; set; }
    }

    public class CountryFull : ICountryID, ICountryCode, ICountryName
    {
        public int CountryID { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }
    }

    #endregion

    #region State

    public class State : IStateID, IStateName
    {
        public int StateID { get; set; }

        public string StateName { get; set; }
    }

    public class StateFull : IStateID,IStateCode, IStateName
    {
        public int StateID { get; set; }

        public string StateCode { get; set; }

        public string StateName { get; set; }
    }

    #endregion

    #region Address Type

    public class AddressType : IAddressTypeID, IAddressTypeName
    {
        public int AddressTypeID { get; set; }

        public string AddressTypeName { get; set; }
    }

    #endregion

    #endregion

    #region User log components

    public class Created : ICreatedBy, ICreatedOn
    {
        public DateTime CreatedOn { get; set; }

        public long CreatedBy { get; set; }
    }

    public class Updated : IUpdatedBy, IUpdatedOn
    {
        public DateTime UpdatedOn { get; set; }

        public long UpdatedBy { get; set; }
    }

    public class CreationDetail : ICreated, IUpdated
    {
        public DateTime CreatedOn { get; set; }

        public long CreatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }

        public long UpdatedBy { get; set; }
    }

    #endregion

    #region Request Components

    public class BasicDetail : IApp, IUserID
    {
        public int AppID { get; set; }

        public long UserID { get; set; }
    }

    #endregion

    #region Gender
        public class GenderDetail : IGenderID, IGenderName
    {
        public int GenderID { get; set; }

        public string GenderName { get; set; }
    }
    #endregion

}
