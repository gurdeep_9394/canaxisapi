﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface.AdminApp
{
    interface ITotalOrders
    {
        int TotalOrders { get; set; }
    }
    interface IPendingOrders
    {
        int PendingOrders { get; set; }
    }
    interface IProcessingOrders
    {
        int ProcessingOrders { get; set; }
    }
    interface ICompletedOrders
    {
        int CompletedOrders { get; set; }
    }
    interface ITotalReturns
    {
        int TotalReturns { get; set; }
    }
    interface IPendingReturns
    {
        int PendingReturns { get; set; }
    }
    interface IProcessingReturns
    {
        int ProcessingReturns { get; set; }
    }
    interface ICompletedReturns
    {
        int CompletedReturns { get; set; }
    }
    interface IAcceptedReturns
    {
        int AcceptedReturns { get; set; }
    }
    interface IInTransitReturns
    {
        int InTransitReturns { get; set; }
    }
    interface IAcceptedOrders
    {
        int AcceptedOrders { get; set; }
    }
    interface IInTransitOrders
    {
        int InTransitOrders { get; set; }
    }
}
