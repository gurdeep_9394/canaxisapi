﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace API.Repo.Auth
{
    public class userAuthentication
    {
        #region User Login

        public DataTable userLoginContact(decimal reqid, int appid, long contact, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userLoginContact", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@Contact", SqlDbType.BigInt).Value = contact;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        public DataTable userLoginUserID(decimal reqid, int appid, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userLoginUserID", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@UserID", SqlDbType.BigInt).Value = userid;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }
        public DataTable userLoginMobileID(decimal reqid, int appid, string mobileid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userLoginMobileID", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd);
                    cmd.Parameters.Add("@RequestID", SqlDbType.Decimal).Value = reqid;
                    cmd.Parameters.Add("@AppID", SqlDbType.Int).Value = appid;
                    cmd.Parameters.Add("@MobileID", SqlDbType.VarChar, 200).Value = mobileid;
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }


        #endregion

        #region User Password Reset

        public int UserAccountPasskeyReset(decimal reqid, int appid, long operand, int typeid, string passkey, string saltkey, string saltkeyiv, long userid, string token, out string msg)
        {
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAccountPasskeyReset", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@TypeID", SqlDbType.Int).Value = typeid;
                    cmd.Parameters.Add("@Passkey", SqlDbType.VarChar, 500).Value = passkey;
                    cmd.Parameters.Add("@Saltkey", SqlDbType.VarChar, 500).Value = saltkey;
                    cmd.Parameters.Add("@SaltkeyIV", SqlDbType.VarChar, 500).Value = saltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region User Password Update

        public int UserAccountPasskeyUpdate(decimal reqid, int appid, long operand, string newpasskey, string newsaltkey, string newsaltkeyiv, string token, long userid, out string msg)
        {
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAccountPasskeyUpdate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@NewPasskey", SqlDbType.VarChar, 500).Value = newpasskey;
                    cmd.Parameters.Add("@NewSaltkey", SqlDbType.VarChar, 500).Value = newsaltkey;
                    cmd.Parameters.Add("@NewSaltkeyIV", SqlDbType.VarChar, 500).Value = newsaltkeyiv;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion

        #region Password Return

        public DataTable userPasskeyReturn(decimal reqid, int appid, string token, long userid, out int status, out string msg)
        {
            DataTable dt = new DataTable();
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    dbObject.Message = string.Empty;
                    SqlCommand cmd = new SqlCommand("userPasskeyReturn", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    dt = dbObject.ReturnTable(cmd);
                    status = Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return dt;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                status = 0;
                return null;
            }
            finally
            {

            }
        }

        #endregion

        #region Token

        public int userAuthenticationTokenCreate(decimal reqid, int appid, string token, long userid, out string msg)
        {
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAuthenticationTokenCreate", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                Helper.RequestHelper sr = new Helper.RequestHelper();
                sr.requestFailAdd(reqid, "userAuthenticationTokenCreate", msg, userid, out msg);
                return 0;
            }
            finally
            {

            }
        }

        public int userAuthenticationTokenDelete(decimal reqid, int appid, int appiddelete, string token, long operand, long userid, out string msg)
        {
            Helper.DatabaseHelper dbObject = new Helper.DatabaseHelper();
            try
            {
                using (SqlConnection connection = new SqlConnection(dbObject.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("userAuthenticationTokenDelete", connection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd = dbObject.ReturnBasic(cmd, userid, token);
                    cmd.Parameters.Add("@Operand", SqlDbType.BigInt).Value = operand;
                    cmd.Parameters.Add("@AppIDDelete", SqlDbType.Int).Value = appiddelete;
                    cmd.ExecuteNonQuery();
                    msg = cmd.Parameters["@Message"].Value.ToString();
                    return Convert.ToInt16(cmd.Parameters["@Stat"].Value);
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                return 0;
            }
            finally
            {

            }
        }

        #endregion
    }
}
