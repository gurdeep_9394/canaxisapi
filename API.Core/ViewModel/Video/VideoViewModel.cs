﻿using API.Core.Interface.ICommon;
using sh.Core.Models.Video;
using System;
using System.Collections.Generic;
using System.Text;

namespace sh.Core.ViewModel
{
    public class VideoViewModel: IResult
    {
        public List<VideoModel> Detail { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
