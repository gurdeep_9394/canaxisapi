﻿using API.Core.Models.Common;
using API.Core.Models.Volume;
using API.Core.ViewModel.Volume;
using API.Repo.Helper;
using API.Repo.Volume;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace dbackers.Controllers
{
    /// <summary>
    /// volume
    /// </summary>
    public class volumeController : ApiController
    {
        /// <summary>
        /// VolumeCreate
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Route("api/volume/VolumeCreate")]
        [HttpPost]

        public IHttpActionResult VolumeCreate(VolumeModel obj)
        {
            Result result = new Result();
            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }
                if (headers.Contains("token"))
                {
                    token = Convert.ToString(headers.GetValues("token").First());
                }
                string message = "";

                dbVolume volume = new dbVolume();
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);


                if (volume.volumeCreate(reqid, appid, obj.Volume, obj.CategoryID, userid, token, out message) == 1)
                {
                    result.Status = 1;
                    result.Message = message;
                    return Content(HttpStatusCode.OK, result);
                }
                else
                {
                    result.Status = 0;
                    result.Message = message;
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                result.Status = 0;
                result.Message = ex.ToString();
                return Content(HttpStatusCode.BadRequest, result);
            }
        }

        /// <summary>
        /// VolumeReturn
        /// </summary>
        /// <param name="volid"></param>
        /// <param name="categoryid"></param>
        /// <param name="typeid"></param>
        /// <returns></returns>
        [Route("api/volume/VolumeReturn")]
        [HttpGet]
        public IHttpActionResult VolumeReturn(int volid, int categoryid,int typeid)
        {
            VolumeViewModel user = new VolumeViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                dbVolume ua = new dbVolume();
                int stat = -1;
                string msg = "";
                List<VolumeDetailModel> Detail = new List<VolumeDetailModel>();
                DataTable detail = ua.volumeReturn(reqid, appid, token, volid, categoryid,typeid,userid, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new VolumeDetailModel
                                  {
                                      TypeID = string.IsNullOrEmpty(item["TypeID"].ToString()) ? 0 : Convert.ToInt32(item["TypeID"]),
                                      TypeName = item.Field<string>("TypeName"),
                                      VolumeID = string.IsNullOrEmpty(item["VolumeID"].ToString()) ? 0 : Convert.ToInt32(item["VolumeID"]),
                                      Volume = item.Field<string>("Volume"),
                                      CategoryID = string.IsNullOrEmpty(item["CategoryID"].ToString()) ? 0 : Convert.ToInt32(item["CategoryID"]),
                                      CategoryName = item.Field<string>("CategoryName")
                                  }).ToList();
                        user.Detail = Detail;
                        user.Message = "Volumn return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.Detail = null;
                        user.Message = "No volumn is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.Detail = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.Detail = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
    }
}
