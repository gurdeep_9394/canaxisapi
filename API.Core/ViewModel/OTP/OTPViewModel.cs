﻿using API.Core.Interface.ICommon;
using API.Core.Models.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.ViewModel.OTP
{
    public class UserOTPViewModel : IResult
    {
        public List<UserOTPDetail> DetailObject { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }

    }
}
