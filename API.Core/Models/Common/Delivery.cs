﻿using API.Core.Interface.ICommon;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Models.Common
{
    public class DeliveryDetail : IDeliveryTypeID, IDeliveryTypeName, IMinValue, IDetail
    {
        public int DeliveryTypeID { get; set; }
        public string DeliveryTypeName { get; set; }
        public string Detail { get; set; }
        public decimal MinValue { get; set; }

    }

  
}
