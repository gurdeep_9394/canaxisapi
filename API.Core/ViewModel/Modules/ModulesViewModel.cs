﻿using API.Core.Interface.ICommon;
using API.Core.Models.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.ViewModel.Modules
{
    public class ModulesViewModel : IResult
    {
        public List<ModulesDetails> Details { get;set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
