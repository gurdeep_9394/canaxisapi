﻿using API.Core.Models.Modules;
using API.Core.ViewModel.Modules;
using API.Repo.Helper;
using API.Repo.Modules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace dbackers.Controllers
{/// <summary>
/// Modules
/// </summary>
    public class ModulesController : ApiController
    {
        /// <summary>
        /// ModulesReturn
        /// </summary>
        /// <param name="moduleID"></param>
        /// <returns></returns>
        [Route("api/Modules/ModulesReturn")]
        [HttpGet]
        public IHttpActionResult ModulesReturn(int moduleID)
        {
            ModulesViewModel user = new ModulesViewModel();

            try
            {
                HttpRequestMessage request = Request;
                HttpRequestHeaders headers = request.Headers;
                int appid = -1;
                long userid = -1;
                string token = "NA";
                if (headers.Contains("appid"))
                {
                    appid = Convert.ToInt32(headers.GetValues("appid").First());
                }

                if (headers.Contains("userid"))
                {
                    userid = Convert.ToInt64(headers.GetValues("userid").First());
                }

                if (headers.Contains("token"))
                {
                    token = headers.GetValues("token").First();
                }
                RequestHelper sr = new RequestHelper();
                decimal reqid = sr.requestUniqueID(userid);
                dbModules ua = new dbModules();
                int stat = -1;
                string msg = "";
                List<ModulesDetails> Detail = new List<ModulesDetails>();
                DataTable detail = ua.moduleReturn(reqid, appid, token, moduleID,  userid, out stat, out msg);
                user.Message = msg;
                user.Status = stat;
                if (stat == 1)
                {
                    if (detail != null)
                    {
                        Detail = (from item in detail.AsEnumerable()
                                  select new ModulesDetails
                                  {
                                      ModulesID = string.IsNullOrEmpty(item["ModulesID"].ToString()) ? 0 : Convert.ToInt32(item["ModulesID"]),
                                      ModulesName = item.Field<string>("ModulesName"),
                                      
                                  }).ToList();
                        user.Details = Detail;
                        user.Message = "Module return successfully!";
                        user.Status = 1;
                    }
                    else
                    {
                        user.Details = null;
                        user.Message = "No Module is found!";
                        user.Status = 1;
                    }
                    return Content(HttpStatusCode.OK, user);
                }
                else
                {
                    user.Details = null;
                    user.Message = msg;
                    user.Status = stat;
                    return Ok(user);
                }
            }
            catch (Exception ex)
            {
                user.Details = null;
                user.Message = ex.Message.ToString();
                user.Status = 0;
                return Content(HttpStatusCode.BadRequest, user);
            }
        }
    }
}
