﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.IUser
{

    public interface IAddressLine1
    {
        string AddressLine1 { get; set; }
    }

    public interface IAddressLine2
    {
        string AddressLine2 { get; set; }
    }

    public interface IPinCode
    {
        long PinCode { get; set; }
    }
}
