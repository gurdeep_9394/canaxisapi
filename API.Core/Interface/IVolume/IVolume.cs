﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.IVolume
{
    interface IVolumeDetail:IVolumeID,IVolume
    {

    }   
    public interface IVolumeID
    {
        int VolumeID { get; set; }
    }
    public interface IVolume
    {
        string Volume { get; set; }
    }
}
