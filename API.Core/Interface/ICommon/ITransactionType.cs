﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface.ICommon
{
    interface ITransactionTypeID
    {
        int TransactionTypeID { get; set; }
    }
    interface ITransactionType
    {
        string TransactionType { get; set; }
    }
}
