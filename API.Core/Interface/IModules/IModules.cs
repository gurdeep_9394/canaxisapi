﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Interface.IModules
{
   public interface IModules: IModulesID, IModulesName
    {
    }
    public interface IModulesID
    {
        int ModulesID { get; set; }
    }
    public interface IModulesName
    {
        string ModulesName { get; set; }
    }
}
